const date = new Date();
var buildDate = date.getTime();
module.exports = {
  publicPath: process.env.VUE_APP_FOLDER,
  devServer: {
    port: 8085,
  },
  configureWebpack: (config) => {
    config.output.filename = 'js/[name].[hash:8].js?v=' + buildDate + '';
    config.output.chunkFilename = 'js/[name].[hash:8].js?v=' + buildDate + '';
  },
  pluginOptions: {
    foo: {
      test: /\.scss$/,
      loader: 'pug-plain-loader',
      use: ['vue-style-loader', 'css-loader', 'sass-loader'],
    },
    i18n: {
      locale: 'es',
      fallbackLocale: 'es',
      localeDir: 'locales',
      enableInSFC: true,
    },
  },
  pwa: {
    name: 'clinte-dashboard',
    short_name: 'clinte-dashboard',
    themeColor: '#183f0b',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    workboxPluginMode: 'InjectManifest',
    display: 'standalone',
    prefer_related_applications: false,
    start_url: '.',
    workboxOptions: {
      swSrc: 'public/sw.js',
      skipWaiting: true,
      clientsClaim: true,
    },
  },
};
