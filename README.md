# vue.js V-DASHBOARD
## Project
```
npm -v <= 12.16.3
@vue/cli
npm install
```
## Crear archivo: .env.local
```
NODE_ENV=local
VUE_APP_I18N_LOCALE=es
VUE_APP_I18N_FALLBACK_LOCALE=es
VUE_APP_URLBASE=http://localhost:7777/
VUE_APP_PORT=7777
```
### local: - env.local
Para conectar al backend local
```
npm run local
```
### alternativa: - env.dev
```
npm run dev
```
