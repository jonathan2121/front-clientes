import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import i18n from './i18n';
import moment from 'moment';
import VueLodash from 'vue-lodash';
import lodash from 'lodash';
import Vuelidate from 'vuelidate';
import Functions from './appscripts/functions';
import Filters from './appscripts/filters';
import Directives from './appscripts/directives';
import VueCookies from 'vue-cookies';
import VueHotkey from 'v-hotkey';
import VueScrollTo from 'vue-scrollto';
import { VueHammer } from 'vue2-hammer';

import vSelect from 'vue-select';
Vue.component('v-select', vSelect);

[
  VueHammer,
  VueScrollTo,
  VueHotkey,
  VueCookies,
  Vuelidate,
  Functions,
  Filters,
  Directives,
].forEach((x) => Vue.use(x));

moment.locale(i18n.locale);
Vue.use(require('vue-moment'), { moment });
Vue.use(VueLodash, { name: 'custom', lodash: lodash });

//global styles
import './assets/fontawesome/css/all.min.css';
const BuefyOptions = {
  defaultIconPack: 'fas',
  defaultFirstDayOfWeek: 1,
  defaultDateFormatter: (date) => {
    return moment(new Date(date)).format('DD-MM-YYYY');
  },
  defaultDateParser: (date) => {
    const m = moment(date).format('YYYY-MM-DD');
    return m.isValid() ? m.toDate() : null;
  },
  defaultDateCreator: (date) => {
    const m = moment(date, 'YYYY-MM-DD').toDate();
    return m;
  },
  defaultMonthNames: moment.months(),
  defaultDayNames: moment.weekdaysShort(),
  defaultNoticeQueue: false,
  defaultModalScroll: 'keep',
  defaultModalCanCancel: ['escape', 'x', 'button'],
};
import Buefy from 'buefy';
Vue.use(Buefy, BuefyOptions);
import './assets/scss/styles.scss';
import './assets/scss/animate.css';

//progress
import VueProgressBar from 'vue-progressbar';
Vue.use(VueProgressBar, {
  color: '#4f8e3a',
  failedColor: '#ff1900',
  thickness: '6px',
});

Vue.config.productionTip = false;

export default new Vue({
  App,
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
