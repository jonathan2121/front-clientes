/* eslint-disable no-console */

import { register } from 'register-service-worker';

const CACHE_NAME = 'static-cache-v1';
// const DATA_CACHE_NAME = "data-cache-v4";
//- `${process.env.BASE_URL}index.html`,

const FILES_TO_CACHE = [];

if (process.env.NODE_ENV === 'prod') {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready() {
      console.log(
        'App is being served from cache by a service worker.\n' +
          'For more details, visit https://goo.gl/AFskqB'
      );
    },
    registered() {
      console.log('Service worker has been registered.');
      caches.open(CACHE_NAME).then((cache) => {
        return cache.addAll(FILES_TO_CACHE);
      });
    },
    cached() {
      console.log('Content has been cached for offline use.');
    },
    updatefound() {
      console.log('New content is downloading.');
      caches.keys().then((keyList) => {
        return Promise.all(
          keyList.map((key) => {
            console.log('[ServiceWorker] Removing old cache', key);
            return caches.delete(key);
          })
        );
      });
    },
    updated() {
      console.log('New content is available; please refresh.');
    },
    offline() {
      console.log(
        'No internet connection found. App is running in offline mode.'
      );
    },
    error(error) {
      console.error('Error during service worker registration:', error);
    },
  });
}
