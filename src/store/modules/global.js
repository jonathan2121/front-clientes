import axios from '@/api/';
import router from '@/router';
import app from '@/main';
const state = {
  sidebarDisplay: true,
  userLoged: null,
  windowWidth: window.innerWidth,
  currentRute: '/',
  config: null,
  hash: '',
  isLoading: false,
};

const getters = {
  getUserLoged: (state) => state.userLoged,
  sidebarDisplay: (state) => state.sidebarDisplay,
  windowWidth: (state) => state.windowWidth,
  getCurrentRute: (state) => state.currentRute,
  getCompany: (state) => state.config,
  isLoading: (state) => state.isLoading,
};

const actions = {
  toggleSidebarDisplay({ commit }, display) {
    commit('setSidebarDisplay', display);
  },
  upadteWindowWidth({ commit }) {
    commit('setWindowWidth');
  },
  setCurrentRute({ commit }) {
    commit('currentRute');
  },
  companyConfig({ commit }) {
    commit('getCompanyConfig');
  },
  hash({ commit }) {
    commit('setHash');
  },
  isLoading({ commit }, mode) {
    commit('isLoading', mode);
  },
};

const mutations = {
  authUser(state, user) {
    state.userLoged = user.data;
    state.permissions = user.permissions;
    app.$cookies.remove('token');
    app.$cookies.set('token', user.Authorization, '40MIN');
    axios.defaults.headers.common['Authorization'] = user.Authorization;
    // app.$router.replace("/"), (error) => console.error(error);
  },
  logoutUser(state) {
    delete state.userLoged;
    delete state.config;
    delete state.permissions;
    app.$cookies.remove('token');
    delete axios.defaults.headers.common['Authorization'];
    if (router.currentRoute.name != 'login')
      app.$router.push({ path: '/login' }), (error) => console.error(error);
    //removelocalstorage
    localStorage.removeItem('vuex');
  },
  async getCompanyConfig(state) {
    try {
      //let res = await apiSession.getCompany();
      // console.warn("getCompanyConfig", company);
      state.config = {
        name: "Automotriz",
        logo: null,
        rif: null,
        codigo: null,
        telefono: null,
        web: null,
      };
      // console.warn(state.config);
    } catch (error) {
      console.error('getCompany@error', error);
    } finally {
      app.$router.replace('/'), (error) => console.error(error);
    }
  },
  setSidebarDisplay: (state, display = null) => {
    state.sidebarDisplay = display;
    const htmlClassName = 'sidebar-is-display';
    if (state.sidebarDisplay) {
      document.documentElement.classList.add(htmlClassName);
    } else {
      document.documentElement.classList.remove(htmlClassName);
    }
  },
  setWindowWidth(state) {
    state.windowWidth = window.innerWidth;
  },
  currentRute(state) {
    state.currentRute = 'nav.' + router.currentRoute.name;
  },
  setHash(state) {
    let date = new Date();
    let time = date.getTime();
    let hash = time;
    state.hash = hash;
  },
  isLoading(state, mode) {
    state.isLoading = mode;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
