import Vue from 'vue';
import Router from 'vue-router';
import store from '../store';
import adminTemplate from '@/layouts/admin';
//import loginTemplate from '@/layouts/login';
//import axiosConfig from '../api';

let rute = (rute) => {
  return () => import(`@/views/${rute}`);
};

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: '/',
      component: adminTemplate,
      meta: {
        requiresAuth: true,
        title: 'admin',
      },
      children: [
        {
          path: '/',
          icon: 'home',
          name: 'home',
          component: rute('home/'),
          meta: { title: 'home' },
        },
        {
          path: '/clientes',
          icon: 'user-friends',
          name: 'customers',
          component: rute('clientes/'),
          meta: { title: 'customers' },
        },
        {
          path: '/consecionario',
          icon: 'car',
          name: 'concessionaire',
          component: rute('consecionario/'),
          meta: { title: 'concessionaire' },
        },
        {
          path: '/controlCliente',
          icon: 'address-book',
          name: 'customerControl',
          component: rute('controlCliente/'),
          meta: { title: 'customerControl' },
        },
        {
          path: '/reporte/clientes',
          icon: 'book',
          name: 'reports',
          component: rute('reporte/clientes/'),
          meta: { title: 'reports' },
        },
      ],
    },
    {
      path: '*',
      component: rute('errors/notfound.vue'),
      meta: {
        title: 'notfound',
      },
    },
    {
      name: '403',
      path: '/403',
      component: rute('errors/forbidden.vue'),
      meta: {
        title: 'forbidden',
      },
    },
    {
      name: 'networkerror',
      path: '/networkerror',
      component: rute('errors/networkerror.vue'),
      meta: {
        title: 'networkerror',
      },
    },
  ],
});

//router
router.afterEach(() => {
  store.dispatch('setCurrentRute');
  if (window.innerWidth < 400) {
    store.commit('setSidebarDisplay', false);
  } else {
    store.commit('setSidebarDisplay', true);
  }
});

export default router;
