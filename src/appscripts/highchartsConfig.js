import i18n from '@/i18n';

import Highcharts from 'highcharts';
import loadHighchartsMore from 'highcharts/highcharts-more.js';
loadHighchartsMore(Highcharts);
import exporting from 'highcharts/modules/exporting.js';
exporting(Highcharts);

Highcharts.setOptions({ lang: i18n.messages[i18n.locale].Highcharts });

export default Highcharts;
