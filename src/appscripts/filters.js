import moment from 'moment';

const Filters = {
  install(Vue) {
    Vue.filter('isEmpty', function (value, fallback) {
      let res = '';
      if (value == null) {
        res = fallback ? fallback : `NULL`;
      } else if (value == undefined) {
        res = fallback ? fallback : `undefined`;
      } else if (value == '') {
        res = fallback ? fallback : `Empty`;
      } else {
        res = value;
      }
      return res;
    });

    Vue.filter('toUSD', function (value) {
      return `$${value}`;
    });

    Vue.filter('uppercase', function (value) {
      return value.toUpperCase();
    });
    Vue.filter('lowercase', function (value) {
      return value.toLowerCase();
    });

    Vue.filter('capitalize', function (value) {
      let val = value.toLowerCase();
      return val.charAt(0).toUpperCase() + val.substr(1);
    });

    Vue.filter('toString', function (value) {
      return value.toString();
    });

    Vue.filter('json', function (value) {
      return JSON.stringify(value);
    });
    Vue.filter('dateFormat', function (value) {
      return moment(value, 'YYYY-MM-DD').format('DD/MM/YYYY');
    });
    Vue.filter('hourFormat', function (value) {
      return moment(value, 'HH:mm:ss').format('HH:mm:ss');
    });
    Vue.filter('timeFormat', function (value) {
      return moment(value, 'YYYY-MM-DD').format('DD/MM/YYYY HH:mm:ss a');
    });
    Vue.filter('map', function (objects, key) {
      return objects.map(function (object) {
        return object[key];
      });
    });
    Vue.filter('numberFormat', function (
      number,
      decimals = 2,
      dec_point = ',',
      thousands_sep = '.'
    ) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = typeof thousands_sep === 'undefined' ? ',' : thousands_sep,
        dec = typeof dec_point === 'undefined' ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k).toFixed(prec);
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    });
  },
};

export default Filters;
