import { ToastProgrammatic as Toast } from 'buefy';
import { SnackbarProgrammatic as Snackbar } from 'buefy';

import app from '../main'; // import the instance
import Store from '../store'; // import the instance

const Functions = {
  install(Vue) {
    Vue.prototype.$toast = function (
      message = 'empty',
      type = 'white',
      position = 'top-right',
      duration = 4000
    ) {
      let fa = 'comment';
      switch (type) {
        case 'success':
          fa = 'thumbs-up';
          break;
        case 'danger':
          fa = 'thumbs-down';
          break;
        case 'warning':
          fa = 'exclamation-triangle';
          break;
        case 'info':
          fa = 'exclamation-circle';
          break;
        default:
          fa = 'comment';
          break;
      }
      let icon = `<i class="fa fa-${fa}"></i> `;
      Toast.open({
        duration: duration,
        message: `${icon}  ${message.toString()}`,
        position: `is-${position}`,
        type: `is-${type}`,
        queue: false,
      });
    };

    Vue.prototype.$snack = function (message = 'empty', type = 'white') {
      Snackbar.open({
        duration: 5000,
        message: message,
        type: `is-${type}`,
        queue: false,
      });
    };
    //- json valores numericos a integer
    Vue.prototype.$jsonObjToInt = function (json) {
      let toString = JSON.stringify(json);
      let resutl = JSON.parse(toString, function (k, v) {
        return typeof v === 'object' || isNaN(v) ? v : parseInt(v, 10);
      });
      return resutl;
    };

    //-- recive un str devuelve un array
    Vue.prototype.$valToArray = function (str) {
      let isArray = str.toString().split(',');
      let a = isArray.filter(Boolean);
      let b = a.map((e) => {
        return parseInt(e);
      });
      let c = b.sort(function (a, b) {
        return a - b;
      });
      return c;
    };

    Vue.prototype.$hasPermission = function (acl) {
      if (acl === undefined) {
        return true;
      } else {
        var arr_acl = acl.split(',');
        var userAcl = Store.state.global.permissions;
        if (userAcl.length > 0) {
          let isValid = userAcl.find((e) => {
            return arr_acl.includes(e);
          });
          return isValid;
        } else {
          return false;
        }
      }
    };
    Vue.prototype.$sortBy = function (array, field) {
      let array_sorted = app.lodash.sortBy(array, [field]);
      return array_sorted;
    };

    Vue.prototype.$numberFormat = function (
      number,
      decimals = 2,
      dec_point = ',',
      thousands_sep = '.'
    ) {
      number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = typeof thousands_sep === 'undefined' ? ',' : thousands_sep,
        dec = typeof dec_point === 'undefined' ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k).toFixed(prec);
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    };

    Vue.prototype.$arrayUnique = function (array) {
      var res = [...new Set(array.map((o) => JSON.stringify(o)))].map((s) =>
        JSON.parse(s)
      );
      return res;
    };

    Vue.prototype.$mergeArrays = function (arrays) {
      var res = [].concat.apply([], arrays);
      return res;
    };

    Vue.prototype.$copy = function (obj) {
      var res = JSON.parse(JSON.stringify(obj));
      return res;
    };
    Vue.prototype.$clearChart = function (object) { //Elimina las comillas de todos los string de cada objeto
      for (const property in object) {
        if (typeof object[property] == "string") {

          object[property] = object[property].replace(/'|"/g, "");        
        }
      }
      return object;
    };
  },
};
export default Functions;
