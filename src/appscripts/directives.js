import app from '../main'; // import the instance
const Directives = {
  install(Vue) {
    Vue.directive('focus', {
      inserted: function (el) {
        el.focus();
      },
    });

    Vue.directive('rand', {
      bind(el) {
        el.style.color = '#' + Math.random().toString().slice(2, 8);
      },
    });

    Vue.directive('acl', {
      inserted(el, binding) {
        if (binding.value) {
          let res = app.$hasPermission(binding.value);
          // console.warn(res)
          if (res) {
            el.style.display = 'INITIAL';
          } else {
            el.style.display = 'NONE';
          }
        }
      },
      update(el, binding) {
        if (binding.value) {
          let res = app.$hasPermission(binding.value);
          // console.warn(res)
          if (res) {
            el.style.display = 'INITIAL';
          } else {
            el.style.display = 'NONE';
          }
        }
      },
    });
  },
};
export default Directives;
